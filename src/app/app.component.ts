import { Component, OnInit } from '@angular/core';
import { Task } from './Task';
import { TodoService } from './services/todo.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  tareas : Task [] =[];
  newTask :string;

  constructor(private todoService : TodoService) {

  }
  ngOnInit(): void {
    this.todoService.get().subscribe(
      result => {
        this.tareas = result.todos;
      },
      error =>{
        console.log(error)
      }
      
    );
  }

  saveTask(){
    if (this.newTask){
      let tareas1 = new Task();
      tareas1.todo = this.newTask;
      tareas1.completed = true
      this.tareas.push(tareas1);
      this.newTask = '';
    }
      else {

        alert('porfavor Ingrese Tarea');
      }
  }

  done(id:number){
    this.tareas[id].completed = !this.tareas[id].completed;
  }

  remove (id:number) {
    this.tareas = this.tareas.filter((v,i)=> i !== id);
  }
}
