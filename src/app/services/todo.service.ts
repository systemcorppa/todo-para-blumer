import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http' 
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor(private httpClient : HttpClient) { }
  
   get(): Observable<any> {

    const headers = {
      headers: {        
        'Content-Type': 'application/json'
      }
    }
    return this.httpClient.get('https://dummyjson.com/todos', headers);
  }
}
